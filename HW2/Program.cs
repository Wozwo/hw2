﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace HW2
{

    /// <summary>
    /// Домашнее задание по Интерфейсам
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }
    interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
    }

    class OtusXmlSerializer<T> : ISerializer<T>
    {
        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();
            T deserialized;
            using (XmlReader reader = XmlReader.Create(stream))
            {
               deserialized = (T)serializer.Deserialize(reader);
            }
            return deserialized;
        }

        public string Serialize<T>(T item)
        {
            throw new NotImplementedException();
        }
    }

    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream stream;
        private ISerializer<T> serializer;

        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var deserialize = serializer.Deserialize<T[]>(stream);
            foreach (var item in deserialize)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    class PersonSorter : ISorter<Person>
    {
        public IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems)
        {
            throw new NotImplementedException();
        }
    }

    class Person
    {

    }
}
